# GAME_NAME
## Overview
### History
HISTORY

### Description
DESCRIPTION

## How to Play
**Controls:**
- CONTROLS

## Credits
### Writing and Programming
[Brandon Vout](https://brandonvout.com/)

### Made In
ENGINE

### LIBRARY_OR_TOOL
LIBRARY_OR_TOOL

### Music
ARTISTS

### Sound Effects
ARTISTS

## Repositories
- [Github](https://github.com/bvout/REPO_NAME)
- [Gitlab](https://gitlab.com/bvout/REPO_NAME)

## License
Published under the **[GNU General Public License v3.0](./LICENSE)**
